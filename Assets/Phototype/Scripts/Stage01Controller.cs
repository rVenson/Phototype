﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage01Controller : MonoBehaviour {

	public int missionID;
	private GameSettings gameSettings;

	public SolarGrid photoA;
	public SolarGrid photoB;
	public SolarGrid photoC;

	public MessagesController message;

	void Start(){

		AudioManager.instance.PlayMusic ("music_titan");

		bool value = false;
		if(GameObject.FindGameObjectWithTag ("GameSettings") != null){
			gameSettings = GameObject.FindGameObjectWithTag ("GameSettings").GetComponent<GameSettings> ();
			gameSettings.missionCompletedList.TryGetValue (missionID, out value);
		}

		//Se o cenário não ficou completo
		if(!value){
			StartCoroutine(InitialSequence ());
		} 
		//Se o cenário já está completo
		else {
			//TODO: Desabilitar sujeira quando o cenário estiver completo
			/*
			photoA.disableDirt = true;
			photoB.disableDirt = true;
			photoC.disableDirt = true;
			*/
		}

        string initialMessage = LocalizationManager.instance.GetLocalizedValue("mission.stage01.entry");

        GameObject.FindGameObjectWithTag ("Canvas")
			.GetComponent<MessagesController>().messageUI.ShowMessage(initialMessage);

	}

	private IEnumerator InitialSequence(){
		List<string> dialog = new List<string> ();

		dialog.Add (LocalizationManager.instance.GetLocalizedValue("mission.stage01.dialog01"));
		dialog.Add (LocalizationManager.instance.GetLocalizedValue("mission.stage01.dialog02"));
        dialog.Add (LocalizationManager.instance.GetLocalizedValue("mission.stage01.dialog03"));
        yield return new WaitForSeconds (5f);
		message.SendDialog (dialog);

        yield return new WaitForSeconds(30f);
        dialog = new List<string>();
        dialog.Add (LocalizationManager.instance.GetLocalizedValue("mission.stage01.dialog04"));
        message.SendDialog(dialog);
    }
}
