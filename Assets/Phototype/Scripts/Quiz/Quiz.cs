﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;

public class Quiz : MonoBehaviour {

	public static Quiz instance = null;

	private Question[] questions;
	public QuizUI quizUI;
	public QuizItem item;

	void Awake(){
		if(instance == null){
			instance = this;
		} else if (instance != this){
			Destroy(gameObject);
		}

        questions = LocalizationManager.instance.GetQuestions();
	}

	public void NewQuestion(Question q, QuizItem item = null){

		if(item != null){
			this.item = item;
		}

		PauseMenu.instance.TransparentPause();

		if(q.questionName == ""){
			item.question = (Question) questions.GetValue(Random.Range(0, questions.Length));
			quizUI.question = item.question;
		} else {
            quizUI.question = q;
		}
		quizUI.gameObject.SetActive(true);
		quizUI.StartQuiz();

	}

	public void CorrectAnswer(){
		if(item != null){
            GameAnalytics.NewDesignEvent("Quiz:CorrectAnswer", 1);
            CanvasController.instance.tipLayer.GetComponent<TipLayer>().ShowText(LocalizationManager.instance.GetLocalizedValue("text.tip.quiz.correct"), 3f, true);
            item.ItemEffect();
		}
	}

	public void FalseAnswer(){
		if(item != null){
            GameAnalytics.NewDesignEvent("Quiz:WrongAnswer", 1);
            CanvasController.instance.tipLayer.GetComponent<TipLayer>().ShowText(LocalizationManager.instance.GetLocalizedValue("text.tip.quiz.wrong"), 3f, true);
            item.ItemBadEffect();
        }
	}

}
