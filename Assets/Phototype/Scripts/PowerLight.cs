﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerLight : MonoBehaviour {

	ScenarioSettings world;
	Light powerLight;

	void Start(){
		world = GameObject.FindGameObjectWithTag ("GameController")
			.GetComponent<ScenarioSettings> ();
		powerLight = gameObject.GetComponent<Light> ();
	}

	void Update () {
		powerLight.intensity = world.worldPower;
	}
}
