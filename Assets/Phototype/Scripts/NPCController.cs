﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCController : MonoBehaviour {

	public string npcName = "NPC Name";
	public Transform player;
	[Space(10)]
	public bool followPlayer = false;

	private NavMeshAgent nav;
	private Animator animator;

	private float vspeed;

	void Start () {
		nav = gameObject.GetComponent<NavMeshAgent> ();
		animator = gameObject.GetComponent<Animator> ();

		vspeed = 0;
	}
	
	void Update () {
		animator.SetFloat ("VSpeed", vspeed);
		if(followPlayer){
			Follow (player);
		}
	}

	public void Wave(){
		animator.SetLayerWeight (1, 1f);
		animator.SetInteger ("CurrentAction", 3);
		Invoke ("StopAction", 1.5f);
	}

	private void StopJumping(){
		animator.SetBool ("Jumping", false);
	}

	private void StopAction(){
		animator.SetInteger ("CurrentAction", 0);
	}

	private void Follow(Transform target){
		nav.SetDestination (target.position);
		vspeed = nav.velocity.magnitude;
	}

	private void RandomMovement(){
		//Vector3 npcPosition = gameObject.transform.position;
		//Vector3 npcDestination = new Vector3 (npcPosition.x + 10, npcPosition.y, npcPosition.z + 10);
		//nav.SetDestination ();
	}

}
