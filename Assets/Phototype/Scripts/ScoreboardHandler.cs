﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;

public class ScoreboardHandler {

    public static List<Score> savedScores = new List<Score>();
    public static bool hasScoreboard = false;

    public static void Save(Score saveData)
    {
        savedScores.Add(saveData);
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/scoreData.gd");
        bf.Serialize(file, savedScores);
        file.Close();

        Debug.Log("Score persistence ok");
    }

    public static void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/scoreData.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/scoreData.gd", FileMode.Open);
            savedScores = (List<Score>)bf.Deserialize(file);
            file.Close();

            hasScoreboard = true;
            Debug.Log("Score load ok");
        }
    }

    public static void Delete()
    {
        if (File.Exists(Application.persistentDataPath + "/scoreData.gd"))
        {
            File.Delete(Application.persistentDataPath + "/scoreData.gd");
        }

    }
}
