﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CompassUI : MonoBehaviour {

	private GameObject player;

	void Start(){
		player = GameObject.FindGameObjectWithTag("Player");
	}

	void Update(){
		float playerDirection = player.transform.eulerAngles.y;
		this.gameObject.transform.eulerAngles = new Vector3(0, 0, playerDirection + 90);
	}
	
}
