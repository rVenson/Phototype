﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScenarioSettings : MonoBehaviour {

	public string scenarioName = "Test";
	public float lat = -28.7823821f;
	public float lon = -49.3703922f;
	public float solarCoeff = 1;
	public float temperature = 25.0f;
	public float worldPower = 0;
	public Vector3 eulerSun;
	public GameObject sun;
	public GameObject wakeUpObject;

	void Start(){
		SceneInit ();
	}

	void FixedUpdate (){
		eulerSun = sun.transform.eulerAngles;
	}

	void SceneInit(){
		wakeUpObject = Instantiate(wakeUpObject, GameObject.FindGameObjectWithTag("Canvas").transform);
		Text text = wakeUpObject.GetComponentInChildren<Text> ();
		text.text = "";

		StartCoroutine (TypeSceneName(text, scenarioName.ToCharArray()));
	}

	private IEnumerator TypeSceneName(Text text, char[] array){

		foreach(char l in array){
			text.text += l.ToString();
			yield return new WaitForSeconds (0.05f);
		}
		yield return new WaitForSeconds (3);
		text.text = "";
		yield return null;
	}

}
