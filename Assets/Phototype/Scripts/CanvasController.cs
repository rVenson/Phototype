﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasController : MonoBehaviour {

	public GameObject damage;
	public GameObject solarGUI;
	public GameObject deathScreen;
	public GameObject tipLayer;
	public GameObject minimalMissionUI;
	public GameObject debugOptions;
	public GameObject settingsDebugUI;

	//CLEANING BAR
	public CleaningUI cleaningBar;
	public float cleaningTime;
	private bool cleaningBarEnabled = false;
	private float barTotalTime = 3f;

	//PANEL
	public SolarPanelMainUI solarMainUI;
	public PlantUI plantUI;

    bool debugMode = false;

    //INSTANCE
    public static CanvasController instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        } else
        {
            Destroy(this);
        }
    }

    void Update(){
        if (debugMode)
        {
            if (Input.GetKeyDown(KeyCode.F6))
            {
                debugOptions.SetActive(true);
                PauseMenu.instance.TransparentPause();
            }

            if (Input.GetKeyDown(KeyCode.F5))
            {
                settingsDebugUI.SetActive(!settingsDebugUI.activeSelf);
            }
        } else {
            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.G))
            {
                debugMode = true;
                settingsDebugUI.SetActive(true);
            }
        }
	}

	//CLEANING BAR

	public void ShowCleaningBar(SolarPanel panel){

		cleaningBar.SetPanel (panel);

		if (!cleaningBarEnabled) {
			StartCoroutine (ShowBar ());
		} else {
			cleaningTime = barTotalTime;
		}
	}

	private IEnumerator ShowBar(){
		cleaningTime = barTotalTime;
		cleaningBarEnabled = true;
		cleaningBar.gameObject.SetActive (true);
		while(cleaningTime > 0){
			cleaningTime -= Time.deltaTime;
			yield return null;
		}
		cleaningBarEnabled = false;
		cleaningBar.gameObject.SetActive (false);
	}

	//PANEL MENU

	public void OpenPanelMenu(SolarPanel solar){
		//panelSolar.RefreshPanel (solar);
		//panelSolar.gameObject.SetActive (true);
	}

	public void OpenMainPanelMenu(SolarPanel solar, bool open){
		solarMainUI.RefreshPanel (solar);
		solarMainUI.gameObject.SetActive (open);
	}
}
