﻿using System.Collections;
using System.Collections.Generic;

public class MissionObjective {

	public string objectiveName;
	public string objectiveDesc;
	public int conditionsLeft;
	public bool isCompleted = false;
}
