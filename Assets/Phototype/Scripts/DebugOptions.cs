﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DebugOptions : MonoBehaviour {

	public void LoadStage(string name){
		SceneManager.LoadScene (name);
	}

    public void SaveProgress(int stage)
    {
        SolarPlant plant = GameObject.FindGameObjectWithTag("SolarPlant").GetComponent<SolarPlant>();
        GameSettings gs = GameSettings.instance;
        World world = World.instance;
        gs.missionCompletedList.Add(stage, true);
        float plantPower = (plant.current * plant.tension);
        gs.missionPower.Add(stage, plantPower);
        float finalTime = Time.time - world.startTime;
        gs.missionTimes.Add(stage, finalTime);
    }
}
