﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunButton : InteractableObject {

	public Cannon cannon;
	public PlumbingFix plumping;

	public AudioSource audioSource;
	public AudioClip negateEffect;
	public AudioClip startEffect;
	public Animator anim;

	private string tipText;

	void Start(){
		AudioManager.instance.RegisterSpatialSource(audioSource);
		//tipText = "Pressione E para ativar o canhão";
        tipText = LocalizationManager.instance.GetLocalizedValue("text.tip.cannonActivation");
	}

	public override string TipText ()
	{
		return tipText;
	}

	public override void triggerAction(){
		if (cannon.charge >= cannon.maxCharge && plumping.isFixed) {
			audioSource.clip = startEffect;
			anim.enabled = true;
			audioSource.Play ();
			cannon.Shoot ();
			isActivated = false;
			StartCoroutine (ButtonLight ());
		} else {
			StopAllCoroutines ();
			audioSource.clip = negateEffect;
			audioSource.Play ();
            //tipText = "O canhão ainda não pode ser ativado!";
            tipText = LocalizationManager.instance.GetLocalizedValue("text.tip.cannonActivationNegative");
            StartCoroutine (RefreshTip());
		}
	}

	void FixedUpdate(){

		if(cannon.charge >= cannon.maxCharge && plumping.isFixed){
			this.GetComponent<Renderer> ().material.color = Color.green;
			isActivated = true;
		}
	}

	private IEnumerator ButtonLight(){
		this.GetComponent<Renderer> ().material.color = Color.yellow;
		yield return new WaitForSeconds (2f);
		this.GetComponent<Renderer> ().material.color = Color.red;
	}

	private IEnumerator RefreshTip(){
		yield return new WaitForSeconds (3f);
		tipText = LocalizationManager.instance.GetLocalizedValue("text.tip.cannonActivation");
    }
}
