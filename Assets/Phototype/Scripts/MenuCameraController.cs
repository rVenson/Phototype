﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuCameraController : MonoBehaviour {

	public GameSettings gameSettings;
	public bool withLerp = true;

	void Start(){
		gameSettings = GameObject.FindGameObjectWithTag ("GameSettings").GetComponent<GameSettings> ();
	}

	public void RotateLeft(){
		StopAllCoroutines();
		StartCoroutine(LerpRotation(-90, 0));
	}

	public void RotateRight(){
		StopAllCoroutines();
		StartCoroutine(LerpRotation(90, 0));
	}

	public void MainMenu(){
		StopAllCoroutines();
		StartCoroutine(LerpRotation(0, 0));
	}

	public void StartGame(){
		StopAllCoroutines();
		StartCoroutine(LerpRotation(0, -90));
	}

	public void StartGameFinalExpert(){
		gameSettings.missionCompletedList.Add (-1, true);
		SceneManager.LoadScene ("ControlRoom");
	}

	public void StartGameFinalNew(){
		SceneManager.LoadScene ("ControlRoom");
	}

	public void QuitGame(){
		Application.Quit ();
	}

	private IEnumerator LerpRotation(float y, float x){

		if (withLerp) {
			float elapsedTime = 0;

			while (elapsedTime < 2) {
				transform.rotation = Quaternion.Lerp (transform.rotation, Quaternion.Euler (new Vector3 (x, y, 0)), Time.deltaTime * 3);
				elapsedTime += Time.deltaTime;
				yield return new WaitForEndOfFrame ();
			}
		} else {
			transform.rotation = Quaternion.Euler (new Vector3 (x, y, 0));
		}
	}

}
