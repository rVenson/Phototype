﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationLight : MonoBehaviour {

	private Light lamp;
	private Renderer rend;

	// Use this for initialization
	void Start () {
		lamp = gameObject.GetComponent<Light> ();
		rend = gameObject.GetComponent<Renderer> ();
	}

	public void StandByStation(){
		lamp.color = Color.yellow;
		rend.material.color = Color.yellow;
	}

	public void ActiveStation(){
		lamp.color = Color.green;
		rend.material.color = Color.green;
	}

	public void DeactiveStation(){
		lamp.color = Color.red;
		rend.material.color = Color.red;
	}
}
