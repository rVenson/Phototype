﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinalStageGUI : MonoBehaviour {

	public Text text;
	public SolarPlant plant;
	public Cannon cannon;

	void FixedUpdate(){
		text.text = LocalizationManager.instance.GetLocalizedValue("label.powerGenerator") + ": " + (plant.current*plant.tension).ToString("F1") + LocalizationManager.instance.GetLocalizedValue("label.powerShort") + "\n" +
            LocalizationManager.instance.GetLocalizedValue("label.cannonPower") + ": " + cannon.charge.ToString("F1") + LocalizationManager.instance.GetLocalizedValue("label.powerShort") + " / " + cannon.maxCharge + LocalizationManager.instance.GetLocalizedValue("label.powerShort");
	}
}
