﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsAudio : MonoBehaviour {

	public Slider musicSlider;
	public Slider efxSlider;
	public Slider spatialSlider;

	void Start(){
		musicSlider.value = AudioManager.instance.musicVolume;
		efxSlider.value = AudioManager.instance.efxVolume;
		spatialSlider.value = AudioManager.instance.spatialVolume;

		MusicListener ();
		EfxListener ();
		SpatialListener ();
	}

	void MusicListener(){
		musicSlider.onValueChanged.AddListener(
			delegate { 
				AudioManager.instance.ChangeMusicVolume(musicSlider.value);
			}
		);
	}

	void EfxListener(){
		efxSlider.onValueChanged.AddListener(
			delegate { 
				AudioManager.instance.ChangeEffectVolume(efxSlider.value);
			}
		);
	}

	void SpatialListener(){
		spatialSlider.onValueChanged.AddListener(
			delegate { 
				AudioManager.instance.ChangeSpatialVolume(spatialSlider.value);
			}
		);
	}
}
