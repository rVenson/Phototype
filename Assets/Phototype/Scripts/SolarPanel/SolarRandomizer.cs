﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolarRandomizer : MonoBehaviour {

	private SolarGrid grid;

	public float percent;
	public bool atLeastOne = true;

	public bool randomizeDirty = false;
	public bool randomizeActivation = false;
	public bool randomizeRoll = false;

	void Awake(){

		grid = this.gameObject.GetComponent<SolarGrid> ();

		//DIRT
		if(randomizeDirty){
			List<SolarPanel> solarList = new List<SolarPanel>();
			foreach(Transform solar in grid.solarList){
				solarList.Add (solar.GetComponent<SolarPanel>());
			}

			int i;
			for(i = 0; i < Random.Range(3, 5); i++){
				SolarPanel solar = solarList [(Random.Range (0, solarList.Count - 1))];
				solar.DirtyPanels ();
				solarList.Remove (solar);
			}
		}

		//ROLL
		if(randomizeRoll){

			List<SolarPanel> solarList = new List<SolarPanel>();
			foreach(Transform solar in grid.solarList){
				solarList.Add (solar.GetComponent<SolarPanel>());
			}

            //int range = (int) (400 / World.instance.eulerSun.eulerAngles.x);
            //range = Mathf.Max(range, 3);

            int range = Random.Range(3, 4);

			int i;
			for(i = 0; i < range; i++){
				SolarPanel solar = solarList [(Random.Range (0, solarList.Count - 1))];
				solar.SetWorstAngle();
				solarList.Remove (solar);
			}
		}
	}

}
