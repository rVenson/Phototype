﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionUI : MonoBehaviour {
	
	public Text title;
	public Text desc;
	public Toggle toggle;

	public void SetTitle(string text){
		title.text = text;
	}

	public void SetDesc(string text){
		desc.text = text;
	}

	public void SetToggle(bool active){
		toggle.isOn = active;
	}

}
