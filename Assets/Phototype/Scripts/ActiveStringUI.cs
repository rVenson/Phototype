﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveStringUI : MonoBehaviour {

    public SolarGrid selectedGrid;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GameObject.FindGameObjectWithTag("Canvas").GetComponent<CanvasController>().plantUI.SetSelectedGrid(selectedGrid);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GameObject.FindGameObjectWithTag("Canvas").GetComponent<CanvasController>().plantUI.SetSelectedGrid(null);
        }
    }
}
