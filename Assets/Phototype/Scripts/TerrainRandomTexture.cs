﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainRandomTexture : MonoBehaviour {

	public Texture2D[] TerrainTextures;
	public TerrainData terraindata;
	public Terrain terrain;

	public void CreateTerrain(){
		/*
		SplatPrototype[] tex = new SplatPrototype [TerrainTextures.Length];
		for (int i=0; i<TerrainTextures.Length; i++) {
			tex [i] = new SplatPrototype ();
			tex [i].texture = TerrainTextures [i];    //Sets the texture
			tex [i].tileSize = new Vector2 (1, 1);    //Sets the size of the texture
		}
		terraindata.splatPrototypes = tex;
		terrain = Terrain.CreateTerrainGameObject (terraindata).GetComponent<Terrain> ();
		*/

		SplatPrototype tex = new SplatPrototype ();
		tex.texture = TerrainTextures[Random.Range(0, TerrainTextures.Length)];
		//tex.tileSize = new Vector2 (2, 2);
		SplatPrototype[] texList = new SplatPrototype[1];
		texList [0] = tex;
		terraindata.splatPrototypes = texList;
		terrain = Terrain.CreateTerrainGameObject (terraindata).GetComponent<Terrain> ();
	}

	void Start () {
		CreateTerrain ();
	}
}
