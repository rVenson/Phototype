﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SerialPanelController : MonoBehaviour {

	public RawImage stateA;
	public RawImage stateB;
	public RawImage stateC;

	public void Active(int station){
		switch (station) {
		case 0:
			{
				stateA.color = Color.green;
				break;
			}
		case 1:
			{
				stateB.color = Color.green;
				break;
			}
		case 2:
			{
				stateC.color = Color.green;
				break;
			}
		default:
			{
				break;
			}
		}
	}

	public void StandBy(int station){
		switch (station) {
		case 0:
			{
				stateA.color = Color.yellow;
				break;
			}
		case 1:
			{
				stateB.color = Color.yellow;
				break;
			}
		case 2:
			{
				stateC.color = Color.yellow;
				break;
			}
		default:
			{
				break;
			}
		}
	}

}
