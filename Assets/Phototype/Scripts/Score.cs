﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Score {

    public System.DateTime date;

	public int alienDeaths = 0;
	public float gameTime = 0f;
	public float totalPower = 0f;

	public int scoreAlien = 0;
	public int scoreTime = 0;
	public int scorePower = 0;

	public int scoreTotal = 0;

	private float multAlien = 100f;
	private float multPower = 0.1f;
	private float multTime = 2000000f;

	public void CalculateScore(){
		scoreTime = (int) Mathf.Clamp(((1 / gameTime) * multTime), 500, 10000);
		scorePower = (int) Mathf.Clamp((totalPower * multPower), 0, 30000);
		scoreAlien = (int) Mathf.Clamp((alienDeaths * multAlien), 0, 15000);

		scoreTotal = (int) (scoreTime + scorePower + scoreAlien);
	}

}
