﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionSystem : MonoBehaviour {

	public int missionID;
	public string missionName;
	public string missionDesc;
	public GameObject missionPanel;
	private GameObject canvas;
	public AudioClip sucessSound;
	private AudioSource sound;
	private MissionUI panel;
	private IGoal actualGoal;
	private GameSettings gameSettings;

	private SolarPlant plant;

	//SCORE
	public float missionStartTime;
	public float missionTime;
	public float missionPower = 0;
	public float missionAliens = 0;

	void Start(){

		missionStartTime = Time.time;

		sound = gameObject.GetComponent<AudioSource> ();
		canvas = GameObject.FindGameObjectWithTag ("Canvas");
		plant = GameObject.FindGameObjectWithTag ("SolarPlant").GetComponent<SolarPlant>();

		bool value = false;

		if(GameObject.FindGameObjectWithTag ("GameSettings") != null){
			gameSettings = GameSettings.instance;
			gameSettings.missionCompletedList.TryGetValue (missionID, out value);
		}

		if(!value){
			Debug.Log ("Iniciando missão " + missionID);
			StartCoroutine ("StartMission");
		}
	}

	void Update(){
        /*
		if(Input.GetKeyDown(KeyCode.N)){
			Debug.Log ("Finalizando missão!");
			gameSettings.missionCompletedList.Add (missionID, true);
			gameSettings.missionTimes.Add (missionID, 40.4f);
			gameSettings.missionPower.Add (missionID, (480 * 100 * 3));
			GameSettings.instance.aliensScore += 2;
		}
        */
	}

	private IEnumerator StartMission(){
		//DELAY
		yield return new WaitForSeconds(5);

		bool objectiveIsCompleted = true;

		//WHILE THE OBJECTIVES ARE NOT COMPLETED
		foreach (Transform child in gameObject.transform) {				
			Debug.Log (child.name);
			objectiveIsCompleted = false;
			GameObject objGoal = child.gameObject;
			objGoal.SetActive (true);
			actualGoal = objGoal.GetComponent<IGoal> ();

			StartCoroutine (ShowPanel(true));

			//START THE OBJECTIVE
			StartCoroutine(actualGoal.StartGoal (done =>{
					if(done){
						Debug.Log("Objetivo completo");
						panel.SetToggle(true);
						sound.clip = sucessSound;
						sound.Play();
						StartCoroutine(ShowPanel(false));
						objectiveIsCompleted = true;
						objGoal.SetActive(false);
					} else {
						Debug.Log("Objetivo não completo");
					}
			}));

			while(!objectiveIsCompleted){
				yield return new WaitForSeconds (1);
			}


			yield return new WaitForSeconds (3);
		}

		//MISSAO COMPLETA
	
		//Calculate score
		missionTime = Time.time - missionStartTime;

		gameSettings.missionCompletedList.Add (missionID, true);
		gameSettings.missionTimes.Add (missionID, missionTime);
		gameSettings.missionPower.Add (missionID, (plant.current * plant.tension));
	}

	private IEnumerator ShowPanel(bool active){
		if (active) {
			//PAINEL
			panel = Instantiate (missionPanel, canvas.transform).GetComponent<MissionUI> ();
			panel.SetTitle (actualGoal.GetName());
			panel.SetDesc (actualGoal.GetDesc());
			panel.SetToggle (false);
		} else {
			yield return new WaitForSeconds (3);
			Destroy (panel.gameObject);
		}
	}

}
