﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;
using GameAnalyticsSDK;

public class PlayerController : MonoBehaviour {

	public int maxHealth = 100;
	public int currentHealth;

	public Camera playerCam;
	public float actionRange = 2.5f;
	public AudioSource voice;
	private float damageDelay = 0f;
	private Image damageImage;
	public bool isAlive = true;

	private Image cursor;
	private float lastHitTime = 0;

	private CanvasController canvas;

	public AudioClip[] damageSounds;
	public AudioClip deathSound;

	void Start () {
		damageImage = GameObject.FindGameObjectWithTag ("Canvas").GetComponent<CanvasController> ().damage.GetComponent<Image>();
		currentHealth = maxHealth;
		cursor = GameObject.FindGameObjectWithTag ("Cursor").GetComponent<Image>();
		canvas = GameObject.FindGameObjectWithTag ("Canvas").GetComponent<CanvasController> ();

		AudioManager.instance.RegisterSpatialSource(voice);

		GetComponent<RigidbodyFirstPersonController>().mouseLook.XSensitivity = GameSettings.instance.mouseSensivity;
		GetComponent<RigidbodyFirstPersonController>().mouseLook.YSensitivity = GameSettings.instance.mouseSensivity;
	
	}

	void Update () {
		PlayerAction ();
		ChangeCursor ();
		Death ();
	}

	private void Crouch(){
		bool crouchButton = Input.GetKey (KeyCode.LeftControl);

		if(crouchButton){
			GetComponent<CapsuleCollider> ().height = 1.2f;
		} else{
			GetComponent<CapsuleCollider> ().height = 1.8f;
		}
	}

	private void PlayerAction(){
		if(Input.GetButtonDown("Action") && !PauseMenu.instance.isPaused){
			Vector3 rayOrigin = playerCam.ViewportToWorldPoint (new Vector3 (0.5f, 0.5f, 0));
			RaycastHit hit;

			//layerMask
			int mask = ~(1 << 8);

			if (Physics.Raycast (rayOrigin, playerCam.transform.forward, out hit, actionRange, mask)) {
				InteractableObject io = hit.collider.gameObject.GetComponent<InteractableObject>();
				if (io != null && io.isActivated) {
						io.triggerAction ();
				}
			}
		}
	}

	private void ChangeCursor(){
		if (cursor != null) {
			Vector3 rayOrigin = playerCam.ViewportToWorldPoint (new Vector3 (0.5f, 0.5f, 0));
			RaycastHit hit;

			int mask = ~(1 << 8);

			if (Physics.Raycast (rayOrigin, playerCam.transform.forward, out hit, actionRange, mask)) {
				InteractableObject io = hit.collider.gameObject.GetComponent<InteractableObject>();
				if (io != null && io.isActivated) {
					cursor.color = Color.blue;
					io.OnMouseEvent ();
                    //canvas.tipLayer.SetActive (true);
                    //canvas.tipLayer.GetComponent<TipLayer> ().SetText (io.TipText());
                    canvas.tipLayer.GetComponent<TipLayer>().ShowText(io.TipText(), 0.05f, false);
				} else {
					cursor.color = Color.red;
					//canvas.tipLayer.SetActive (false);
				}
			} else {
				cursor.color = Color.red;
				//canvas.tipLayer.SetActive (false);
			}
		}
	}

	private void Death(){
		if(currentHealth <= 0 && isAlive){
            GameAnalytics.NewDesignEvent("Gameplay:Death", 1);
            isAlive = false;
			StartCoroutine(DeathAnimation ());
		}
	}

	private IEnumerator DeathAnimation(){
		PlayDeathSound ();

		RigidbodyFirstPersonController controller = gameObject.GetComponent<RigidbodyFirstPersonController> ();
		controller.enabled = false;
		gameObject.GetComponent<Collider> ().enabled = false;
		Time.timeScale = 0.3f;

		yield return new WaitForSeconds (0.3f);

		canvas.deathScreen.SetActive (true);

		float i = 0f;
		Vector3 initPos = gameObject.transform.eulerAngles;
		//Animação
		while(i < 1){
			Vector3 deathPos = new Vector3(0, 0, -90);
			Vector3 newPos = Vector3.Lerp(initPos, deathPos, i);
			gameObject.transform.eulerAngles = newPos;

			i += 0.01f;
			yield return null;
		}
	}

	public void Damage(int damage){
		if(Time.time - lastHitTime > damageDelay){
			damageImage.GetComponent<Animator> ().SetTrigger("DamageTrigger");
			PlayDamageSound ();
			currentHealth -= damage;
			lastHitTime = Time.time;
		}
	}

	public void AddLife(int life){
		currentHealth = Mathf.Clamp(currentHealth + life, 0, maxHealth);
	}

	private void PlayDamageSound(){
		voice.clip = damageSounds [Random.Range (0, damageSounds.Length)];
		voice.pitch = Random.Range (0.9f, 1.05f);
		voice.Play ();
	}

	private void PlayDeathSound(){
		voice.Stop ();
		voice.clip = deathSound;
		voice.pitch = Random.Range (0.8f, 0.9f);
		voice.Play ();
	}
}
