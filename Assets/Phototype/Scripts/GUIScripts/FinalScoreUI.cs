﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using GameAnalyticsSDK;

//Deprecated
public class FinalScoreUI : MonoBehaviour {

	public Text totalTimeText;
	public Text totalPowerText;
	public Text totalAliensText;

	public Text scoreTimeText;
	public Text scorePowerText;
	public Text scoreAliensText;

	public Text finalScoreText;

	private float totalTime;
	private float totalPower;
	private float totalAliens;

	//Multiplicadores
	const float multTime = 1000000.0f;
	const float multPower = 0.2f;
	const float multAliens = 100.0f;

	private GameSettings gameSettings;

	void Start(){
		if(GameObject.FindGameObjectWithTag ("GameSettings") != null){

            Debug.Log("Calculando Score!");

			gameSettings = GameObject.FindGameObjectWithTag ("GameSettings").GetComponent<GameSettings> ();
			CalculatePoints();
			PrintPoints ();

            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "FinalScore", "Time", (int) totalTime);
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "FinalScore", "Power", (int) totalPower);
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "FinalScore", "Aliens", (int) totalAliens);
        }
	}

	private void CalculatePoints(){
		float value = 0;

		//Mission Time
		totalTime = 0;
		gameSettings.missionTimes.TryGetValue (1, out value);
		totalTime += value;
		value = 0;

		gameSettings.missionTimes.TryGetValue (2, out value);
		totalTime += value;
		value = 0;

		gameSettings.missionTimes.TryGetValue (3, out value);
		totalTime += value;
		value = 0;

		//MissionPower
		totalPower = 0;
		gameSettings.missionPower.TryGetValue (1, out value);
		totalPower += value;
		value = 0;

		gameSettings.missionPower.TryGetValue (2, out value);
		totalPower += value;
		value = 0;

		gameSettings.missionPower.TryGetValue (3, out value);
		totalPower += value;
		value = 0;

		//Aliens
		totalAliens = GameSettings.instance.CalculateAliens();
	}

	private void PrintPoints(){

		int scoreTime = (int) ((1 / totalTime) * multTime);
		int scorePower = (int)(totalPower * multPower);
		int scoreAliens = (int)(totalAliens * multAliens);

		int scoreTotal = (int) (scoreTime + scorePower + scoreAliens);


		totalTimeText.text = totalTime.ToString("0") + " " + LocalizationManager.instance.GetLocalizedValue("label.seconds");
		totalPowerText.text = totalPower.ToString("0") + " " + LocalizationManager.instance.GetLocalizedValue("label.powerUnit");
		totalAliensText.text = totalAliens.ToString("0") + " " + LocalizationManager.instance.GetLocalizedValue("label.aliens");

		scoreTimeText.text = scoreTime.ToString("0") + " " + LocalizationManager.instance.GetLocalizedValue("label.points");
		scorePowerText.text = scorePower.ToString("0") + " " + LocalizationManager.instance.GetLocalizedValue("label.points");
		scoreAliensText.text = scoreAliens.ToString("0") + " " + LocalizationManager.instance.GetLocalizedValue("label.points");

		finalScoreText.text = scoreTotal.ToString("0") + " " + LocalizationManager.instance.GetLocalizedValue("label.points");
	}
}
