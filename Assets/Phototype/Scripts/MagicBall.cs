﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicBall : MonoBehaviour {

	void Start(){
		AudioManager.instance.RegisterSpatialSource (gameObject.GetComponentInChildren<AudioSource>());
	}

	void OnTriggerEnter(Collider other){
		if(other.CompareTag("Player")){
			Debug.Log ("Magicball hits the player");
			other.GetComponent<PlayerController> ().Damage (20);
		}
	}
}
