﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToogleObject : MonoBehaviour {

	public GameObject obj;

	void OnEnable(){

		obj.SetActive (!obj.activeInHierarchy);
	
	}
}
