﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameAnalyticsSDK;

public class ScoreSequenceUI : MonoBehaviour {

	public Text alienText;
	public Text timeText;
	public Text powerText;

	public Text alienScoreText;
	public Text timeScoreText;
	public Text powerScoreText;

	public Text finalScore;

	void Start(){

		Score score = GameSettings.instance.CalculateScore ();

        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "FinalScore", "Time", (int) score.gameTime);
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "FinalScore", "Power", (int) score.totalPower);
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "FinalScore", "Aliens", (int) score.alienDeaths);

        alienText.text = score.alienDeaths.ToString("0 '"+ LocalizationManager.instance.GetLocalizedValue("label.aliens") + "'");
		timeText.text = score.gameTime.ToString("0's'");
		powerText.text = (score.totalPower / 1000).ToString("0'kW'");

		alienScoreText.text = score.scoreAlien.ToString("'+'0");
		timeScoreText.text = score.scoreTime.ToString("'+'0");
		powerScoreText.text = score.scorePower.ToString("'+'0");

		finalScore.text = score.scoreTotal.ToString ("0' "+ LocalizationManager.instance.GetLocalizedValue("label.points") + "'");

	}

}
