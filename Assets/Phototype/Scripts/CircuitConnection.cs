﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircuitConnection {

	public SolarPanelController c1;
	public SolarPanelController c2;
	public bool type; //true: serial; false: paralel

	public CircuitConnection(SolarPanelController c1, SolarPanelController c2, bool type){
		this.c1 = c1;
		this.c2 = c2;
		this.type = type;
	}

	public float GetCurrent(){
		float current = 0;
		if (type) {
			current = Mathf.Max (c1.actualA, c2.actualA);
		} else {
			current = c1.actualA + c2.actualA;
		}
		return current;
	}

	public float GetTension(){
		float tension = 0;
		if (type) {
			tension = c1.actualV + c2.actualV;
		} else {
			tension = Mathf.Max (c1.actualV, c2.actualV);
		}
		return tension;
	}

}
