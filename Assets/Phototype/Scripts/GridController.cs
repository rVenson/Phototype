﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridController : MonoBehaviour {

	public string gridName = "Planta";
	public bool disableDirt;

	private SolarPanelController[] photopanels;
	public float finalTension;
	public float finalCurrent;

	public bool randomAngle = false;
	public float randomPercent = 0.3f;

	public SolarPanelController solarSelected = null;

	void Start(){
		photopanels = gameObject.GetComponentsInChildren<SolarPanelController> ();

		//id panels
		int id = 0;

		SolarPanelController solarFixed = (SolarPanelController) photopanels.GetValue (Random.Range(0, photopanels.Length));

		//Random
		foreach(SolarPanelController solar in photopanels){
			solar.idPanel = id + 1;
			id++;
			if (randomAngle) {
				if(solarFixed == solar || Random.value < randomPercent){
					solar.inclination = Random.Range(0, 50);
					solar.orientation = Random.Range(0, 360);
					solar.roll.eulerAngles = new Vector3 (0, solar.orientation, solar.inclination);
				} else {
					solar.inclination = 30;
					solar.orientation = 280;
					solar.roll.eulerAngles = new Vector3 (0, solar.orientation, solar.inclination);
				}
			} else {
				solar.inclination = 30;
				solar.orientation = 280;
				solar.roll.eulerAngles = new Vector3 (0, solar.orientation, solar.inclination);
			}
		}
	}

	void FixedUpdate(){
		CalcPower ();
	}

	private void CalcPower(){
		finalTension = 0;
		finalCurrent = 0;

		foreach(SolarPanelController panel in photopanels){
			finalTension += panel.actualV;
			finalCurrent += panel.actualA;
		}
	}
}
