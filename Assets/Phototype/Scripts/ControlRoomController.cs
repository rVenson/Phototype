﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;

public class ControlRoomController : MonoBehaviour {

	public int missionID;
	private GameSettings gameSettings;

	public GameObject mainPlayer;
	public GameObject multigun;
	public GameObject gunItem;
	public DoorHandler door;

	bool sceneCompleted = false;

	void Awake(){

		gameSettings = GameSettings.instance;
		gameSettings.missionCompletedList.TryGetValue (missionID, out sceneCompleted);

		//Se o cenário não ficou completo ou veio do menu principal
		if(!sceneCompleted){

			door.active = false;

			bool valueMenu;
			//Testa se veio do menu principal
			gameSettings.missionCompletedList.TryGetValue (-1, out valueMenu);

			if(valueMenu){
				multigun.SetActive(true);
				gunItem.SetActive (false);

				//Porta
				door.active = true;

				gameSettings.missionCompletedList.Add (missionID, true);
			}
				
		} 
		//Se o cenário já está completo
		else {
            mainPlayer.transform.position = new Vector3 (1f, 1.1f, 14.6f);
			Vector3 newRotation = new Vector3 (0, -90, 0);
			mainPlayer.transform.eulerAngles = newRotation;

			//Activate gun
			multigun.SetActive(true);
			gunItem.SetActive (false);

			//Porta
			door.active = true;
		}
	}

	void Start(){
		if(!sceneCompleted){
            string initialMessage = LocalizationManager.instance.GetLocalizedValue("mission.contromRoomStage.entry");

			GameObject.FindGameObjectWithTag ("Canvas")
				.GetComponent<MessagesController>().messageUI.ShowMessage(initialMessage);
		}
	}
}
