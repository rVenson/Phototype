﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFoot : MonoBehaviour {

	public AudioSource foot;

	void Start(){
		AudioManager.instance.RegisterSpatialSource(foot);
	}

}
