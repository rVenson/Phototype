﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TeleportActivator : InteractableObject {

	private GameObject teleportUI;

	void Start(){
		teleportUI = GameObject.FindGameObjectWithTag ("Canvas").transform.Find ("TeleportUI").gameObject;
	}

	public override void triggerAction ()
	{
        //Se o control room n esta disponivel
        if (SceneManager.sceneCountInBuildSettings > 2)
        {
            teleportUI.SetActive(true);
        } else
        {
            SceneManager.LoadScene("MainMenu");
        }     
	}

}
