﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathScreen : MonoBehaviour {

	void OnEnable(){
		PauseMenu pause = GameObject.FindGameObjectWithTag ("Canvas").GetComponent<PauseMenu>();
		pause.TransparentPause ();
		AudioManager.instance.ChangePitch ();
	}

	public void RestartGame() {
		SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex);
		Time.timeScale = 1;
		AudioManager.instance.RestaurePitch ();
	}
}