﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;

public class ObjectiveTutorial03 : Objective {

	public DoorHandler door;
	public TeleportController teleport;

	public override IEnumerator ObjectiveTrigger(){

        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "Tutorial", "Objective03");

        MissionSystem2.stageClear = true;

		Debug.Log ("Pegue o teleport");
		ShowMessages ();

		door.active = true;
		door.OpenDoor();

		while (true) {
			if(teleport.isUsed){

                GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "Tutorial", "Objective03");

                sucess = true;
				OnObjectiveSucess ();
				SaveProgress ();

				break;
			}
			yield return new WaitForSeconds (1);
		}

		yield return null;
	}

	public void ShowMessages(){
		MessagesController messages = GameObject.FindGameObjectWithTag ("Canvas").GetComponent<MessagesController> ();

		List<string> dialog = new List<string> ();
        dialog.Add(LocalizationManager.instance.GetLocalizedValue("mission.contromRoom.objective03.dialog01"));
        dialog.Add(LocalizationManager.instance.GetLocalizedValue("mission.contromRoom.objective03.dialog02"));
        dialog.Add(LocalizationManager.instance.GetLocalizedValue("mission.contromRoom.objective03.dialog03"));
        messages.SendDialog (dialog);
	}

	private void SaveProgress(){
		GameSettings gs = GameSettings.instance;
		gs.missionCompletedList.Add (0, true);
	}
}
