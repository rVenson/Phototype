﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsSequence : MonoBehaviour {

	public Transform sequenceObjects;

	void Start () {
		StartCoroutine (Sequence());
	}

	private IEnumerator Sequence(){

		GameObject last = null;

		foreach(Transform g in sequenceObjects){
			yield return new WaitForSeconds (15f);
			if(last != null){
				last.SetActive (false);
			}
			g.gameObject.SetActive (true); 
		}
	}
}
