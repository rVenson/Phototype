﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;

public class ObjetiveTutorial02 : Objective {

	public SolarPanel p1;
	public SolarPanel p2;

	public override IEnumerator ObjectiveTrigger(){

        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "Tutorial", "Objective02");

        Debug.Log ("Limpe paineis");
		ShowMessages ();

		while(!sucess){

			if (p1.cleaning == 1 && p2.cleaning == 1) {
				sucess = true;
				OnObjectiveSucess ();
				Debug.Log ("Finalizado");
                GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "Tutorial", "Objective02");
                break;
			}

			yield return new WaitForSeconds (1.0f);
		}

		yield return null;
	}

	public void ShowMessages(){
		MessagesController messages = GameObject.FindGameObjectWithTag ("Canvas").GetComponent<MessagesController> ();

		List<string> dialog = new List<string> ();
        dialog.Add(LocalizationManager.instance.GetLocalizedValue("mission.contromRoom.objective02.dialog01"));
        dialog.Add(LocalizationManager.instance.GetLocalizedValue("mission.contromRoom.objective02.dialog02"));
        dialog.Add(LocalizationManager.instance.GetLocalizedValue("mission.contromRoom.objective02.dialog03"));
        dialog.Add(LocalizationManager.instance.GetLocalizedValue("mission.contromRoom.objective02.dialog04"));
        messages.SendDialog (dialog);
	}
}
