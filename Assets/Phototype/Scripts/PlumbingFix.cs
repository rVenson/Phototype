﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlumbingFix : InteractableObject {

	public bool startDamaged = true;
	public bool isFixed = false;
	public GameObject fire;
	public Animator anim;

	public MeshRenderer mesh;
    private string defaultTipText;
    private string tipText;

	public override string TipText ()
	{
		return tipText;
	}

	void Start(){

        defaultTipText = LocalizationManager.instance.GetLocalizedValue("text.tip.plumbingFix");
        tipText = defaultTipText;

        if (startDamaged){
			DestroyPlumbing ();
		} else{
			FixPlumbing ();
		}
	}

	public override void triggerAction(){
		if (!fire.activeSelf) {
			//mesh.enabled = true;
			anim.enabled = true;
			isFixed = true;
			isActivated = false;
			Debug.Log ("Completed");
		} else {
			tipText = LocalizationManager.instance.GetLocalizedValue("text.tip.plumbingFixNegative");
            StopAllCoroutines ();
			StartCoroutine (RestartTip());
		}
	}

	public void DestroyPlumbing(){
		fire.SetActive (true);
		mesh.enabled = false;
		isFixed = false;
	}

	public void FixPlumbing(){
		fire.SetActive (false);
		mesh.enabled = true;
		isFixed = true;
	}

	private IEnumerator RestartTip(){
		yield return new WaitForSeconds (3);
        tipText = defaultTipText;
	}

}
