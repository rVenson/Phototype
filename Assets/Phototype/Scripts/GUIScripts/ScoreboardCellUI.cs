﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreboardCellUI : MonoBehaviour {

    public Text finishDate;
    public Text gameTime;
    public Text gamePower;
    public Text gameAliens;
    public Text gameTotal;

}
