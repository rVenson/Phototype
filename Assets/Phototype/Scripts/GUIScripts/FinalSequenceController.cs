﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinalSequenceController : MonoBehaviour {

	public GameObject descSequence;
	public GameObject scoreSequence;
	public GameObject creditsSequence;
	public GameObject lastSequence;

	private bool isButton = false;

	void Start(){
		StartCoroutine(InitFinalSequence());
	}

	void Update(){
		if(Input.anyKey){
			isButton = true;
		}
	}

	private IEnumerator InitFinalSequence(){
		yield return new WaitForSeconds (3);
		descSequence.SetActive (true);
		yield return new WaitForSeconds (3);
		while(!isButton){
			yield return new WaitForSeconds(0.2f);
		}
		isButton = false;
		descSequence.SetActive (false);
		scoreSequence.SetActive (true);
		yield return new WaitForSeconds (3);

		while(!isButton){
			yield return new WaitForSeconds(0.2f);
		}
		isButton = false;
		scoreSequence.SetActive (false);
		creditsSequence.SetActive (true);
		yield return new WaitForSeconds (3);

		while(!isButton){
			yield return new WaitForSeconds(0.2f);
		}
		isButton = false;
		creditsSequence.SetActive (false);
		lastSequence.SetActive (true);
		yield return new WaitForSeconds (3);

		while(!isButton){
			yield return new WaitForSeconds(0.2f);
		}

		SceneManager.LoadScene ("MainMenu");
	}
}
