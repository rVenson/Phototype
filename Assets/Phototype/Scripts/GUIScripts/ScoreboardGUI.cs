﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreboardGUI : MonoBehaviour {

    public GameObject scoreCellObject;
    public Transform content;

    public GameObject scoreBoard;
    public GameObject noScoreBoard;
    public GameObject confirmation;

    private List<Score> scores;

    void Awake()
    {
        ScoreboardHandler.Load();
    }

    void Start()
    {
        if (ScoreboardHandler.hasScoreboard)
        {
            RefreshUI();
            scoreBoard.SetActive(true);
        } else
        {
            noScoreBoard.SetActive(true);
        }
    }

    public void RefreshUI()
    {
        foreach (Score s in ScoreboardHandler.savedScores)
        {
            GameObject scoreCell = Instantiate(scoreCellObject, content);
            ScoreboardCellUI cell = scoreCell.GetComponent<ScoreboardCellUI>();

            cell.finishDate.text = s.date.ToShortDateString();
            cell.gameTime.text = s.gameTime.ToString("F0");
            cell.gamePower.text = s.totalPower.ToString("F0");
            cell.gameAliens.text = s.alienDeaths.ToString("F0");
            cell.gameTotal.text = s.scoreTotal.ToString("F0");
        }
    }

    public void ResetConfirmation()
    {
        confirmation.SetActive(true);
    }

    public void CancelReset()
    {
        confirmation.SetActive(false);
    }

    public void ResetBoard()
    {
        ScoreboardHandler.Delete();
        scoreBoard.SetActive(false);
        noScoreBoard.SetActive(true);
        confirmation.SetActive(false);
    }
}
