﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TipLayer : MonoBehaviour {

	public Text tipText;
    public bool isLocked = false;

	public void SetText(string tip){
        StopAllCoroutines();
		tipText.text = tip;
	}

    // Executa apenas se: 1 - tiplayer estiver destravado; 
    // 2 - For uma requisição do tipo lockTip (trava o layer até que suma)
    public void ShowText(string tip, float seconds, bool lockTip)
    {
        if (!isLocked || lockTip) {
            if (lockTip)
            {
                isLocked = true;
            }
            StopAllCoroutines();
            tipText.text = tip;
            gameObject.SetActive(true);
            StartCoroutine(DisableTip(seconds));
        }
    }

    IEnumerator DisableTip(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        gameObject.SetActive(false);
        isLocked = false;
    }
}
