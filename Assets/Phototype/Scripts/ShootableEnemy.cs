﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootableEnemy : MonoBehaviour {

	public int currentHealth = 50;
	public bool changeColor = true;
	public GameObject deathAnimation;
	public TextMesh lifeText;
	public AudioSource deathSound;

	private Color originalColor;
	private Renderer rend;

	private int maxHealth;
	private Animator anim;

	public void Start(){
		AudioManager.instance.RegisterSpatialSource (deathSound);

		anim = gameObject.GetComponent<Animator> ();

		maxHealth = currentHealth;
		UpdateHealth ();
	}

	public void Damage(int damageAmount){
		currentHealth -= damageAmount;
		Debug.Log (gameObject.GetInstanceID() + " received " + damageAmount + " of damage");
		UpdateHealth ();
		if (currentHealth <= 0) {
			if(deathAnimation != null){
				deathAnimation.SetActive (true);
				DeathSound ();
			}
			StartCoroutine (Unspawn());
		} else {
			//setcolor
			if (changeColor) {
				StartCoroutine (DamageColor ());
			}
		}
	}

	private IEnumerator DamageColor(){
		rend = GetComponent<Renderer> ();
		Color originalColor = rend.material.color;
		rend.material.color = Color.red;
		yield return new WaitForSeconds (0.3f);
		rend.material.color = originalColor;

	}

	private IEnumerator Unspawn (){
		//rg.AddExplosionForce(100, rg.position, 10);
		anim.enabled = false;  

		if(changeColor){
			rend.material.color = Color.clear;
		}
		yield return new WaitForSeconds (3);
		Destroy (gameObject);
	}

	private void UpdateHealth(){
		lifeText.text = "Vida: " + currentHealth + "/" + maxHealth;
	}

	private void DeathSound(){
		if(deathSound != null){
			Debug.Log ("Playing death sound");
			deathSound.Play ();
		}
	}
}
