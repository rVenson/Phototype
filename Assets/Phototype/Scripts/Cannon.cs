﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour {

	public GameObject gunNozzle;
	public BossController boss;
	public SolarPlant plant;

	private AudioSource audioSource;

	public float maxCharge = 64;
	public float charge;
	private LineRenderer laserLine;

	void Start(){
		audioSource = this.GetComponent<AudioSource> ();
		laserLine = gunNozzle.GetComponent<LineRenderer> ();
		charge = 0;

		AudioManager.instance.RegisterSpatialSource(audioSource);

		StartCoroutine (Charger());
	}

	void Update(){
        /*
		if(Input.GetKeyDown(KeyCode.O)){
			Shoot ();
		}
        */
	}

	private IEnumerator Charger(){
		while(true){
			if(charge < maxCharge){
				charge += (plant.current * plant.tension)/60/60;
			} else {
				charge = maxCharge;
			}
			yield return new WaitForSeconds(1);
		}
	}

	public void Shoot(){
		StartCoroutine (ShootAnim ());
	}

	private IEnumerator ShootAnim(){
		charge = 0;
		Debug.Log ("Laser has been fired");
		laserLine.enabled = true;
		audioSource.Play ();
		boss.Hit ();
		yield return new WaitForSeconds (3f);
		laserLine.enabled = false;
	}

}
