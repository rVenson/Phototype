﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using GameAnalyticsSDK;

public class GameSettings : MonoBehaviour {

	public Dictionary<int, bool> missionCompletedList = new Dictionary<int, bool>();
	public Dictionary<int, float> missionTimes = new Dictionary<int, float>();
	public Dictionary<int, float> missionPower = new Dictionary<int, float>();

	public Dictionary<string, int> aliensScore = new Dictionary<string, int>();
	public float mouseSensivity = 1f;

	private Score score;

	public float area25Lat;
    public float area25Lon;

	public static GameSettings instance = null;

	public float startTime;

	void Awake(){

        if (instance == null) {
			instance = this;
            GameAnalytics.Initialize();
        } else {
			Destroy(this.gameObject);
		}


		DontDestroyOnLoad (this);
		score = new Score ();

		//Define lat lon
		List<float[]> area25range = new List<float[]>();
        //USA
        area25range.Add(new float[] {Random.Range(30, 50), Random.Range(-80, -100)});
        //Africa NORTH
        area25range.Add(new float[] {Random.Range(6, 24), Random.Range(-4, -38)});
        //NORTH EUROPEASIA
        area25range.Add(new float[] {Random.Range(40, 65), Random.Range(15, 180)});
        //AUSTRALIA
        area25range.Add(new float[] {Random.Range(-15, -35), Random.Range(120, 150)});

        //TEST
        //area25range.Add(new float[] { Random.Range(0, 0), Random.Range(0, 0) });

        int selectedRange = Random.Range(0, area25range.Count);

		area25Lat = area25range[selectedRange][0];
		area25Lon = area25range[selectedRange][1];

		Debug.Log("Area25: Lat " + area25Lat + " Lon " + area25Lon);
	}

	void Start(){
		startTime = Time.time;
	}

	public Score CalculateScore(){
		score.alienDeaths = CalculateAliens();
		score.gameTime = CalculateTime ();
		score.totalPower = CalculatePower ();
		score.CalculateScore ();

        score.date = System.DateTime.Now;

		Debug.Log ("Score do alien: " + score.scoreAlien);

        ScoreboardHandler.Save(score);

		return score;
	}

	private int CalculateTime(){
		float value = 0;

		//Mission Time
		float total = 0;
		missionTimes.TryGetValue (1, out value);
		total += value;
		value = 0;

		missionTimes.TryGetValue (2, out value);
		total += value;
		value = 0;

		missionTimes.TryGetValue (3, out value);
		total += value;
		value = 0;

		missionTimes.TryGetValue (4, out value);
		total += value;
		value = 0;

		return (int) total;
	}

	private int CalculatePower(){
		float value = 0;

		//Mission Time
		float total = 0;
		missionPower.TryGetValue (1, out value);
		total += value;
		value = 0;

		missionPower.TryGetValue (2, out value);
		total += value;
		value = 0;

		missionPower.TryGetValue (3, out value);
		total += value;
		value = 0;

		return (int) total;
	}

    public int CalculateAliens()
    {
        int aliens = 0;
        foreach (int a in aliensScore.Values)
        {
            aliens += a;
        }

        return aliens;
    }

    public void GameRestart()
    {
        Destroy(gameObject);
        SceneManager.LoadScene("MainMenu");
    }
}
