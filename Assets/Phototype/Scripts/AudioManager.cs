﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class AudioManager : MonoBehaviour {

	public float efxVolume = 0.3f;
	public float spatialVolume = 1f;
	public float musicVolume = 0.2f;

	public Sound[] effectList;
	public Sound[] musicList;
	private List<AudioSource> spatialList = new List<AudioSource>();

	public static AudioManager instance = null;

	void Awake ()
	{
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (gameObject);

		DontDestroyOnLoad (gameObject);

		foreach(Sound s in effectList){
			s.source = gameObject.AddComponent<AudioSource> ();
			s.source.playOnAwake = false;
			s.source.clip = s.clip;
			//s.source.volume = s.volume;
			s.source.volume = efxVolume;
			s.source.pitch = s.pitch;
			s.source.loop = s.loop;
		}

		foreach(Sound s in musicList){
			s.source = gameObject.AddComponent<AudioSource> ();
			s.source.playOnAwake = false;
			s.source.clip = s.clip;
			//s.source.volume = s.volume;
			s.source.volume = musicVolume;
			s.source.pitch = s.pitch;
			s.source.loop = s.loop;
		}
	}

	public void RegisterSpatialSource(AudioSource source){
		spatialList.Add (source);
		source.volume = spatialVolume;
	}

	public AudioSource PlayEffect(string name){
		Sound s = Array.Find (effectList, sound => sound.name == name);
		if (s == null) {
			Debug.LogWarning ("Sound " + name + " not found!");
			return null;
		}

		s.source.Play ();

		return s.source;
	}

	public AudioSource StopEffect(string name){
		Sound s = Array.Find (effectList, sound => sound.name == name);
		if (s == null) {
			Debug.LogWarning ("Sound " + name + " not found!");
			return null;
		}

		s.source.Stop ();

		return s.source;
	}

	public AudioSource PlayMusic(string name){

		//Para todas as musicas primeiro
		foreach (Sound music in musicList) {
			music.source.Stop ();
		}

		Sound s = Array.Find (musicList, sound => sound.name == name);
		if (s == null) {
			//Debug.LogWarning ("Music " + name + " not found!");
			return null;
		}

		s.source.Play ();

		return s.source;
	}

	public void ChangeMusicVolume(float volume){
		musicVolume = volume;

		foreach(Sound s in musicList){
			s.source.volume = volume;
		}
	}

	public void ChangeEffectVolume(float volume){
		efxVolume = volume;

		foreach(Sound s in effectList){
			s.source.volume = volume;
		}
	}

	public void ChangeSpatialVolume(float volume){
		spatialVolume = volume;

		AudioSource[] aux = spatialList.ToArray ();
		foreach(AudioSource s in aux){
			if(s != null){
				s.volume = volume;
			} else {
				Debug.Log ("Removendo audisource nulo");
				spatialList.Remove(s);
			}
		}
	}

    public void PauseAllSounds()
    {
        foreach (Sound s in effectList)
        {
            s.source.Pause();
        }

        foreach (AudioSource s in spatialList)
        {
            if (s != null)
            {
                s.Pause();
            }
        }
    }

    public void UnPauseAllSounds()
    {
        foreach (Sound s in effectList)
        {
                s.source.UnPause();
        }

        foreach (AudioSource s in spatialList)
        {
            if (s != null)
            {
                s.UnPause();
            }
        }
    }

    public void LoopMusic(string name, bool loop){
		Sound s = Array.Find (musicList, sound => sound.name == name);
		if (s == null) {
			Debug.LogWarning ("Sound " + name + " not found!");
			return;
		}

		s.source.loop = loop;
	}

	public void ChangePitch(){
		foreach(Sound s in musicList){
			s.source.pitch = 2f;
		}
	}

	public void RestaurePitch(){
		foreach(Sound s in musicList){
			s.source.pitch = 1f;
		}
	}
}