﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageEnd : MonoBehaviour, IGoal {

	bool sucess = false;
	string goalName = "Volta pra casa";
	string goalDesc = "Volte para o laboratório em segurança";
	string goalLongDesc = "Utilize o teletransporte para retornar ao laboratório principal";

	public TeleportController teleport;

	public IEnumerator StartGoal(System.Action<bool> done){

		while (true) {
			if (sucess) {
				done (true);
				break;
			}
			//Debug.Log ("Verificando " + sucess);
			yield return new WaitForSeconds (1);
		}
	}

	void FixedUpdate(){
		if(teleport.isUsed){
			sucess = true;
		}
	}

	public void FinalizeGoal(){
		sucess = true;
	}

	public string GetName(){
		return goalName;
	}

	public string GetDesc(){
		return goalDesc;
	}

	public string GetLongDesc(){
		return goalLongDesc;
	}

	public bool GetCompleted(){
		return sucess;
	}
}
