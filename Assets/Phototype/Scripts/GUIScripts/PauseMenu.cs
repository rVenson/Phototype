﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityStandardAssets.ImageEffects;

public class PauseMenu : MonoBehaviour {

	public GameObject pauseMenu;
	public bool isPaused = false;

	public static PauseMenu instance = null;

	void Awake(){
		instance = this;
	}

	void Update(){
		if(Input.GetKeyDown(KeyCode.Escape)){
			PauseGame ();
		}
	}

	public void PauseGame(){

		if (isPaused) {
			//Esta pausado
			Time.timeScale = 1;
            AudioManager.instance.UnPauseAllSounds();
            pauseMenu.SetActive (false);
			Camera.main.GetComponent<BlurOptimized>().enabled = false;
			Camera.main.GetComponent<VignetteAndChromaticAberration>().enabled = false;
			LockCursor (true);
		} else {
			//Esta rodando
			Time.timeScale = 0;
            AudioManager.instance.PauseAllSounds();
			pauseMenu.SetActive (true);
			Camera.main.GetComponent<BlurOptimized>().enabled = true;
			Camera.main.GetComponent<VignetteAndChromaticAberration>().enabled = true;
			LockCursor (false);
		}
			
		isPaused = !isPaused;
	}

	public void TransparentPause(bool pause){
		if (!pause) {
			Time.timeScale = 1;
            AudioManager.instance.UnPauseAllSounds();
            Camera.main.GetComponent<BlurOptimized>().enabled = false;
			LockCursor (true);
		} else {
			Time.timeScale = 0;
            AudioManager.instance.PauseAllSounds();
            Camera.main.GetComponent<BlurOptimized>().enabled = true;
			LockCursor (false);
		}

		isPaused = !isPaused;
	}

	public void TransparentPause(){
		if (isPaused) {
			Time.timeScale = 1;
            AudioManager.instance.UnPauseAllSounds();
            Camera.main.GetComponent<BlurOptimized>().enabled = false;
			LockCursor (true);
		} else {
			Time.timeScale = 0;
            AudioManager.instance.PauseAllSounds();
            Camera.main.GetComponent<BlurOptimized>().enabled = true;
			LockCursor (false);
		}

		isPaused = !isPaused;
	}

	private void LockCursor(bool option){
		if (option) {
			GameObject player = GameObject.FindGameObjectWithTag ("Player");
			RigidbodyFirstPersonController controller = player.GetComponent<RigidbodyFirstPersonController> ();
			controller.mouseLook.lockCursor = true;
			controller.enabled = true;
			Cursor.visible = false;
		} else {
			GameObject player = GameObject.FindGameObjectWithTag ("Player");
			RigidbodyFirstPersonController controller = player.GetComponent<RigidbodyFirstPersonController> ();
			controller.mouseLook.lockCursor = false;
			Cursor.lockState = CursorLockMode.None;
			controller.enabled = false;
			Cursor.visible = true;
		}
	}

}
