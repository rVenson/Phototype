﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SolarPanelController : InteractableObject {

	public float idPanel = 0;
	public float inclination = 0;
	public float orientation = 0;
	public float maxA = 8.69f; //current
	public float maxV = 36.8f; //tension
	public float actualA;
	public float actualV;
	public float cleaning = 1;
	public float solarIncidence = 1;

	public GameObject dirtCell;
	public bool isDirt = false;

	private ScenarioSettings world;
	private GridController grid;
	private GameObject solarGUI;
	private GameObject myPanelGUI;

	private GameObject canvas;

	private bool isPanelActive = false;
	private GameObject[] dirtCells = new GameObject[4];
	private AudioSource audioSource;

	public AudioClip audioLerp;
	public AudioClip audioLock;

	public Transform roll;
	public Transform dirt;

	//lerp
	private float startTime;
	private bool isLerping = false;
	private Vector3 endPosition;
	private Vector3 startPosition;
	private float timeTakenDuringLerp = 10f;

	void Awake(){
		canvas = GameObject.FindGameObjectWithTag ("Canvas");
		solarGUI = canvas.GetComponent<CanvasController> ().solarGUI;

		AudioManager.instance.RegisterSpatialSource(audioSource);

		world = GameObject.FindGameObjectWithTag ("GameController").GetComponent<ScenarioSettings> ();
		grid = gameObject.GetComponentInParent<GridController> ();
		audioSource = gameObject.GetComponent<AudioSource> ();
	}

	void Start(){
		startTime = Time.time;

		if(isDirt && !grid.disableDirt){
			if(Random.value < 0.3){
				DirtyPanels ();
			}
		}
	}

	void Update(){

		if(isPanelActive){

			myPanelGUI.GetComponent<SolarGUIController> ().RefreshPanel (this);

			if (Input.GetKey (KeyCode.Period)) {
				StartCoroutine(InclinateRotate(0, 1));
			} else if (Input.GetKey (KeyCode.Comma)) {
				StartCoroutine(InclinateRotate(0, -1));
			}

			if(Input.GetKey(KeyCode.RightBracket)){
				StartCoroutine(InclinateRotate(1, 0));
			} else if(Input.GetKey(KeyCode.LeftBracket)){
				StartCoroutine(InclinateRotate(-1, 0));
			}
		}
	}

	void FixedUpdate(){
		CalculateEletricalFactors ();
		RotorLerping ();
	}

	private void DirtyPanels(){
		Vector3[] pos = new Vector3[4];
		pos [0] = new Vector3 (0.25f, 0, 0.25f);
		pos [1] = new Vector3 (0.25f, 0, -0.25f);
		pos [2] = new Vector3 (-0.25f, 0, 0.25f);
		pos [3] = new Vector3 (-0.25f, 0, -0.25f);

		int i = 0;
		foreach(Vector3 p in pos){
			dirtCells[i] = Instantiate (dirtCell, dirt, false);
			dirtCells[i].transform.localPosition = p;
			i++;
		}
	}

	public override void triggerAction(){

		if (isPanelActive) {
			isPanelActive = false;
			Destroy (myPanelGUI);
			StartRotor ();
			grid.solarSelected = null;
		} else {
			if (grid.solarSelected == null) {
				isPanelActive = true;
				myPanelGUI = Instantiate (solarGUI, canvas.transform);
				myPanelGUI.GetComponent<SolarGUIController> ().RefreshPanel (this);
				grid.solarSelected = this;
			} else {
				//powerIn = grid.solarSelected;
				//grid.solarSelected.triggerAction ();
				//kill console
				grid.solarSelected.triggerAction();
				this.triggerAction ();
			}
		}
	}

	public void Cleaning(){
		cleaning = 0;

		foreach(GameObject cell in dirtCells){
			if(cell == null){
				cleaning += 0.25f;
			}
		}
	}

	private void CalculateIncidence(){
		//FIXME: SOLAR ANGLE

		Transform sun = world.sun.transform;
		Transform obj = gameObject.transform;
		solarIncidence = Vector3.Angle (sun.forward, obj.up) / 180;

		//solarIncidence = gameObject.transform.up;
	}

	private void CalculateEletricalFactors(){
		Cleaning ();
		CalculateIncidence ();

		//calculate
		actualA = maxA * solarIncidence * cleaning;
		actualV = maxV * solarIncidence * cleaning;

		//FIXME: world power?
		//defineworld
		//world.worldPower = actualA / 2;
	}

	private IEnumerator InclinateRotate(float addInclination, float addRotation){
		if(inclination+addInclination >= 0 && inclination+addInclination <=50){
			inclination += addInclination;
		}

		if(orientation+addRotation >= 0 && orientation+addRotation <= 360){
			orientation += addRotation;
		}

		yield return null;
	}

	public void StartRotor(){
		isLerping = true;
		startTime = Time.time;

		startPosition = roll.eulerAngles;

		endPosition = new Vector3 (0, orientation, inclination); //aqui vai o valor definido na GUI

		Debug.Log (endPosition);

		timeTakenDuringLerp = (endPosition - startPosition).magnitude * 0.1f;

		audioSource.clip = audioLerp;
		audioSource.Play();
	}

	private void RotorLerping(){

		//FIXME: o rotor dá uma volta completa quando a rotação encontra-se em 360

		if(isLerping){
			float timeSinceStarted = Time.time - startTime;
			float percentageComplete = timeSinceStarted / timeTakenDuringLerp;
			roll.eulerAngles = Vector3.Lerp (startPosition, endPosition, percentageComplete);

			if(percentageComplete >= 1f){
				isLerping = false;
				audioSource.Stop ();
				audioSource.clip = audioLock;
				audioSource.Play ();
			}
		}
	}
}
